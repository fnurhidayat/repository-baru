const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;

require('./models');

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: 'Hello World'
  });
});

app.listen(PORT, () => {
  console.log(`Server started at ${PORT}`);
})